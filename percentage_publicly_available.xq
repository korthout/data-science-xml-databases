let $papers := doc("eprints-2015-02-01.xml")/eprints/paper
let $groups := distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/research_groups)
return (
	for $significantGroup in (
		for $group in $groups
		let $papersByGroup := (
			for $paper in $papers
			where $paper/research_groups = $group
			return $paper
		)
		where count($papersByGroup) >= 10
		order by $group
		return $group
	)
	let $significantPapers := (
		for $paper in $papers
		where $paper/research_groups = $significantGroup
		return $paper
	)
	let $papersWithFullTextStatus := (
		for $paper in $significantPapers
		where exists($paper/full_text_status)
		return $paper
	)
	let $publicPapers := (
		for $paper in $papersWithFullTextStatus
		where ($paper/full_text_status = 'public')
		return $paper
	)
	let $percentage := (
		count($publicPapers) 
		div count($significantPapers) 
		* 100
	)
	order by $percentage
	return
		<publicity>
			<group>{$significantGroup}</group>
			<number-of-papers>{count($significantPapers)}</number-of-papers>
			<public-papers>{count($publicPapers)}</public-papers>
			<percentage>{$percentage}</percentage>
		</publicity>
)