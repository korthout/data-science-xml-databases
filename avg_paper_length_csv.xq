for $group in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/research_groups)
let $papers := (for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
    where ($paper/research_groups = $group)
    return $paper)
let $length := avg(for $p in $papers
    return $p/pages)
order by $length    
return 
concat($group, ":", round($length), "&#10;")
