for $group in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/research_groups)
let $papers := 
    (for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
    where ($paper/research_groups = $group and exists($paper/abstract))
    return $paper
    )
let $years := 
    ("2009","2010","2011","2012","2013") 
let $avg := avg(
    for $year in $years
    let $papersInYear := (
        for $paper in $papers
        where (
            substring($paper/date_issue, 1, 4) = $year
            or substring($paper/date_effective, 1, 4) = $year 
        )
        return $paper
    )
    order by $year
    return count($papersInYear)
)    
 
let $papers_in_2014 :=
    count(for $paper in $papers
     where (
            substring($paper/date_issue, 1, 4) = "2014"
            or substring($paper/date_effective, 1, 4) = "2014"
        )
    return $paper)
where $avg >= 5
let $change := ($papers_in_2014 div $avg)
order by $change
return 
<change_rate>
    <group>{$group}</group>
    <rate>{substring(string($change),0,5)}</rate>
</change_rate>

