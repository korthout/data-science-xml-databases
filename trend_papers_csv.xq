let $papers := doc("eprints-2015-02-01.xml")/eprints/paper
let $years := distinct-values(
	for $paper in $papers
	let $year := (
		if (string-length($paper/date_issue) >=  4) then substring($paper/date_issue, 1, 4)
		else if (string-length($paper/date_effective) >= 4) then substring($paper/date_effective, 1, 4)
		else 'unknown'
	)
	return $year
)

for $group in distinct-values($papers/research_groups)
let $papersByGroup := (
	for $paper in $papers
	where $paper/research_groups = $group
	return $paper
)
let $papersInYears := (
	for $year in $years
	let $papersInYear := (
		for $paper in $papersByGroup
		where (
			substring($paper/date_issue, 1, 4) = $year
			or substring($paper/date_effective, 1, 4) = $year
			or (
				'unknown' = $year 
				and (
					string-length($paper/date_issue) <  4
					or string-length($paper/date_effective) <  4
				)
			)
		)
		return $paper
	)
	order by $year
	return concat(count($papersInYear), ":")
)
let $orderedYears := (
	for $year in $years
	order by $year
	return concat($year, ":")
)
return
	concat($group, ":", string-join($orderedYears), string-join($papersInYears))
