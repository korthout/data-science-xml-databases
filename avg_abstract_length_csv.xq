for $group in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/research_groups)
let $papers := (for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
    where ($paper/research_groups = $group and exists($paper/abstract))
    return $paper)
let $abstracts := avg(for $abs in $papers/abstract
    return count(tokenize($abs, "\s+")))
order by $abstracts    
return 
concat($group, ":", round($abstracts), "&#10;")
