# Data Science Topic 1 XML Databases Project Group 7
**By Sybe van Hijum and Nico Korthout**

All the code can be run by using Basex. The first step to execute any query is to load the eprints XML database file. This can be done using the command: CHECK [path to Eprints file]. All files can then be executed using the command: RUN [path to file].

##Basic queries
###Average paper length
To get the average paper length for each group in pages avg_paper_length.xq can be used. This returns XML structured data. avg_paper_length_csv.xq returns the same results as colon separated output. This can be read using a spreadsheet program as a CSV file using colon as the column separator. For example, Google Spreadsheet can be used to do this.

###Average abstract length
To get the average abstract length for each group in words, avg_abstract_length.xq can be used. This returns XML structured data. avg_abstract_length_csv.xq returns the same results as colon separated output. This can be read using a spreadsheet program as a CSV file using colon as the column separator. For example, Google Spreadsheet can be used to do this.

###Public available paperes
The query in percentage_publicly_available_papers.xq returns all groups that have published at least 10 papers, together with the number of papers published, the number of papers that are publicly available and the percentage of papers publicly available. percentage_publicly_available_papers_csv.xq returns the same results as a colon separated output.  This can be read using a spreadsheet program as a CSV file using colon as the column separator. For example, Google Spreadsheet can be used to do this.

##Graph queries
###GraphML output
For the GraphML output two queries have to be executed. First nodes.xq has to be executed, this returns all nodes in graphml format. Second edges.xq has to be executed. This returns all edges in graphml format, representing collaborating groups in a paper. The output then needs to be combined in a GraphML file. At the top of this file the following has to be added:

```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <graph id="G" edgedefault="undirected">
```
Then the nodes have to be added, and following the edges.  The file has to be closed by the closing tags for the graph.
```
#!xml
  </graph>
</graphml>
```

##Trend queries
###Rate of group growth
The query in rate_group_growth.xq returns all groups that have published an average of 5 papers in the period 2009-2013. For these groups a rate is given, that shows the rate between the number of papers published in 2014 and the average between 2009 and 2013. The query in rate_group_growth_csv.xq shows the same output as colon separated output. This can be read using a spreadsheet program as a CSV file using colon as the column separator. For example, Google Spreadsheet can be used to do this.

###Trend papers
The query in trend_papers.xq returns all research groups, together with all years that are present in the database. For each research group it shows how many papers have been published in that year. The query in trend_papers_csv.xq returns the same results as colon separated output. This can be read using a spreadsheet program as csv file with the colon as column separator. This can be read using a spreadsheet program as a CSV file using colon as the column separator. For example, Google Spreadsheet can be used to do this. This output is structured by first showing the group name, then all years, and finally all published papers for those years.