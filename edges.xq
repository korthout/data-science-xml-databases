for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
for $group1 in $paper/research_groups
for $group2 in $paper/research_groups
where $group1 > $group2
return <edge source="{$group1}" target="{$group2}"/>
