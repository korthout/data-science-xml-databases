for $group in distinct-values(doc("eprints-2015-02-01.xml")/eprints/paper/research_groups)
let $papers := (for $paper in doc("eprints-2015-02-01.xml")/eprints/paper
	where ($paper/research_groups = $group)
	return $paper)
let $length := avg(for $paper in $papers
	return $paper/pages)
order by $length    
return
	<avg>
		<group>{$group}</group>
		<length>{$length}</length>
	</avg>